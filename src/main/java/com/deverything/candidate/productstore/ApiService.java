package com.deverything.candidate.productstore;

import java.util.List;

import com.deverything.candidate.productstore.model.BoxListObject;
import com.deverything.candidate.productstore.model.CheckoutObject;
import com.deverything.candidate.productstore.model.CheckoutSummaryObject;
import com.deverything.candidate.productstore.model.ProductDimensionsObject;
import com.deverything.candidate.productstore.model.ProductObject;



public interface ApiService {

    /**
     * Should get a json list of products from API
     */
    public List<ProductObject> getProducts() throws Exception;

    /**
     * Should get a json object back with width and heigh for a given productId
     * @throws Exception 
     */
    public ProductDimensionsObject getProductDimensions(int productId) throws Exception;

    /**
     * Should get a json object with a list of boxes from the API
     * @return
     * @throws Exception 
     */
    public List<BoxListObject> getBoxes() throws Exception;

    /**
     * Performs the checkout
     * @param checkoutObject json object that contains the boxId and the list of products to checkout
     * @return the checkout summary object
     * @throws Exception 
     */
    public CheckoutSummaryObject checkout(CheckoutObject checkoutObject) throws Exception;
    /**
     * Should Get List of products that have price higher than passed price
     * @param products json object 
     * @param price
     * @return the List<ProductObject object
     */
	public List<ProductObject> getProductsPriceHigherX(List<ProductObject> products, Integer price);
    /**
     * Should test wich box best fit products
     * @param boxListObject json object that contains the boxId and the list of products to checkout
     * @param productObjectsList json object that contains the boxId and the list of products to checkout
     * @return the BoxListObject object
     */
	public BoxListObject boxFitsProducts(List<BoxListObject> boxListObject, List<ProductDimensionsObject> productObjectsList);
}
