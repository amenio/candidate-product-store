package com.deverything.candidate.productstore.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductsResponse {
  @JsonProperty("statusCode")
  String statusCode;
  @JsonProperty("products")
  List<ProductObject> products;
}