package com.deverything.candidate.productstore.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BoxListObject {
	  @JsonProperty("id")
	  Integer id;
	  @JsonProperty("width")
	  Integer width;
	  @JsonProperty("height")
	  Integer height;

		public int getVolume() {
			return this.height * this.width * this.width;
			
		}
		
		public PairFitPosition canFitProduct(ProductDimensionsObject product) {
			PairFitPosition fitProduct= new PairFitPosition();
			fitProduct.setCanFit(false);
			fitProduct.setNormalPositon(false);
			if(this.width >= product.getWidth() && this.height >= product.getHeight())
			{
				fitProduct.setCanFit(true);
				fitProduct.setNormalPositon(true);
			}
			else if (this.width >= product.getHeight() && this.height >= product.getWidth())
			{  
				fitProduct.setCanFit(true);
				fitProduct.setNormalPositon(false);
			}
			return fitProduct;
		}
}
