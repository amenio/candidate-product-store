package com.deverything.candidate.productstore.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BoxesResponse {

	  @JsonProperty("statusCode")
	  String statusCode;
	  @JsonProperty("boxes")
	  List<BoxListObject> boxes;
}
