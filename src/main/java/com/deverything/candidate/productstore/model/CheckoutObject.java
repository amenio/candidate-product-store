package com.deverything.candidate.productstore.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder
public class CheckoutObject {

	  @JsonProperty("boxId")
	  @NonNull Integer boxId;
	  @JsonProperty("productsIds")
	  @NonNull List<Integer> productsIds;
}
