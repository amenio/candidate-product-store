package com.deverything.candidate.productstore.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductObject {

	  @JsonProperty("id")
	  Integer id;
	  @JsonProperty("name")
	  String name;
	  @JsonProperty("price")
	  Integer price;
}
