package com.deverything.candidate.productstore.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckoutSummaryObject {
	  @JsonProperty("statusCode")
	  String statusCode;
	  @JsonProperty("result")
	  String result;
}
