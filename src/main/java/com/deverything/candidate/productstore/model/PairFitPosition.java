package com.deverything.candidate.productstore.model;

import lombok.Data;

@Data
public class PairFitPosition {
 Boolean canFit;
 Boolean normalPositon;
}
