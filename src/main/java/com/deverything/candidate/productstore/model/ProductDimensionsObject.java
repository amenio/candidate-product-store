package com.deverything.candidate.productstore.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDimensionsObject {
	  @JsonProperty("statusCode")
	  String statusCode;
	  @JsonProperty("width")
	  Integer width;
	  @JsonProperty("height")
	  Integer height;
	  
	  public int getVolume() {
			return this.height * this.width * this.width;
			
	  }
	  
	  public List<BoxListObject> splitBox(BoxListObject box,Boolean samePosition)
	  {
		  final BoxListObject BoxX = new BoxListObject();
		  final BoxListObject BoxY = new BoxListObject();
		  final BoxListObject BoxZ = new BoxListObject();
		  if (samePosition)
		  {
			  BoxX.setId(box.getId());
		      BoxX.setHeight(box.getHeight());
		      BoxX.setWidth(box.getWidth()-this.width);
		      BoxY.setId(box.getId());
		      BoxY.setHeight(box.getHeight());
		      BoxY.setWidth(box.getWidth()-this.width);
		      BoxZ.setId(box.getId());
			  BoxZ.setHeight(box.getHeight()- this.height);
			  BoxZ.setWidth(box.getWidth());
		  }else {
		      BoxX.setId(box.getId());
		      BoxX.setHeight(box.getHeight());
		      BoxX.setWidth(box.getWidth()- this.height);
		      BoxY.setId(box.getId());
		      BoxY.setHeight(box.getHeight());
		      BoxY.setWidth(box.getWidth()-this.width);
		      BoxZ.setId(box.getId());
			  BoxZ.setHeight(box.getHeight()- this.width);
			  BoxZ.setWidth(box.getWidth());
		}
		  return Arrays.asList(BoxX, BoxY, BoxZ);
	  }
}
