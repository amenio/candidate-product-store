package com.deverything.candidate.productstore;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.http.HttpEntity;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.deverything.candidate.productstore.model.BoxListObject;
import com.deverything.candidate.productstore.model.BoxesResponse;
import com.deverything.candidate.productstore.model.CheckoutObject;
import com.deverything.candidate.productstore.model.CheckoutSummaryObject;
import com.deverything.candidate.productstore.model.ProductDimensionsObject;
import com.deverything.candidate.productstore.model.ProductObject;
import com.deverything.candidate.productstore.model.ProductsResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service(value = "apiService")
public class ApiServiceImpl implements ApiService {
	private static final String API_USER = "API_USER";
	private static final String API_KEY = "API_KEY";
	private static final String API_URL = "API_URL";
	private static final Logger LOGGER = LogManager.getLogger(ApiServiceImpl.class);
	private static final BasicHeader HEADER_CONTENT_TYPE_JSON = new BasicHeader("Content-Type", "application/json");
	@Autowired
	private Environment env;

    /**
     * Should get a json list of products from API
     */
	@Override
	public List<ProductObject> getProducts() throws Exception {
		String url = "/products";
		Request request = buildGetRequest(url);
		ProductsResponse productObject = Executor.newInstance().execute(request).handleResponse(r -> {
			int statusCode = r.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				try {
					ObjectMapper objectMapper = new ObjectMapper();
					objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
					return objectMapper.readValue(r.getEntity().getContent(), ProductsResponse.class);
				} catch (Exception e) {
					LOGGER.error("Failed to parse response for:" + url, e);
					return null;
				}
			}
			LOGGER.error("API " + url + " returned StatusCode[" + statusCode + "]");
			throw new HttpClientErrorException(HttpStatus.valueOf(statusCode));
		});
		return productObject.getProducts();
	}
	 /**
     * Should Get List of products that have price higher than passed price
     * @param products json object 
     * @param price
     * @return the List<ProductObject object
     */
	@Override
	public List<ProductObject> getProductsPriceHigherX(List<ProductObject> products, Integer price) {
		return products.stream().filter(p -> p.getPrice() > price).collect(Collectors.toList());
	}
	 /**
     * Should get a json object back with width and heigh for a given productId
     * @throws Exception 
     */
	@Override
	public ProductDimensionsObject getProductDimensions(int productId) throws Exception {
		String url = "/products/" + productId;
		Request request = buildGetRequest(url);
		return (ProductDimensionsObject) Executor.newInstance().execute(request).handleResponse(r -> {
			int statusCode = r.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				try {
					ObjectMapper objectMapper = new ObjectMapper();
					objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
					return objectMapper.readValue(r.getEntity().getContent(), ProductDimensionsObject.class);
				} catch (Exception e) {
					LOGGER.error("Failed to parse response for:" + url, e);
					return null;
				}
			}
			LOGGER.error("API " + url + " returned StatusCode[" + statusCode + "]");
			throw new HttpClientErrorException(HttpStatus.valueOf(statusCode));
		});
	}
	/**
     * Should get a json object with a list of boxes from the API
     * @return
     * @throws Exception 
     */
	@Override
	public List<BoxListObject> getBoxes() throws Exception {
		String url = "/boxes";
		Request request = buildGetRequest(url);
		BoxesResponse boxesResponse = Executor.newInstance().execute(request).handleResponse(r -> {
			int statusCode = r.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				try {
					ObjectMapper objectMapper = new ObjectMapper();
					objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
					return objectMapper.readValue(r.getEntity().getContent(), BoxesResponse.class);
				} catch (Exception e) {
					LOGGER.error("Failed to parse response for:" + url, e);
					return null;
				}
			}
			LOGGER.error("API " + url + " returned StatusCode[" + statusCode + "]");
			throw new HttpClientErrorException(HttpStatus.valueOf(statusCode));
		});
		return boxesResponse.getBoxes();
	}
	 /**
     * Should test wich box best fit products
     * @param boxListObject json object that contains the boxId and the list of products to checkout
     * @param productObjectsList json object that contains the boxId and the list of products to checkout
     * @return the BoxListObject object
     */
	@Override
	public BoxListObject boxFitsProducts(List<BoxListObject> boxListObject,
			List<ProductDimensionsObject> productObjectsList) {
		// first step remove all boxes that can't contain the demanded products
		BoxListObject fitBox = null;
		final int productVolume = this.getSumVolume(productObjectsList);
		List<BoxListObject> boxFitsProducts = boxListObject.stream().filter(b -> b.getVolume() >= productVolume)
				.collect(Collectors.toList());
		// Second step sort boxes from small to big and products from big to small
		boxFitsProducts.sort(Comparator.comparing(BoxListObject::getVolume));
		productObjectsList.sort(Comparator.comparing(ProductDimensionsObject::getVolume, Comparator.reverseOrder()));
		// step 3: Go through all boxes and try to put provided products
		for (BoxListObject box : boxFitsProducts) {
			this.fillBox(box, productObjectsList);
			if (productObjectsList.size() == 0) {
				fitBox = box;
				break;
			}
		}
		return fitBox;
	}
    /**
     * Performs the checkout
     * @param checkoutObject json object that contains the boxId and the list of products to checkout
     * @return the checkout summary object
     * @throws Exception 
     */
	@Override
	public CheckoutSummaryObject checkout(CheckoutObject checkoutObject) throws Exception {
		String url = "/checkout";
		Request request = buildPostRequest(checkoutObject, url);
		return checkObject(request, url, checkoutObject);
	}
	
	private Request buildGetRequest(String apiUrl) {
		String host = env.getProperty(API_URL);
		String user = env.getProperty(API_USER);
		String key = env.getProperty(API_KEY);
		String endpoint = host + apiUrl;
		return Request.Get(endpoint).addHeader(HEADER_CONTENT_TYPE_JSON).addHeader(new BasicHeader("USER", user))
				.addHeader(new BasicHeader("APIKEY", key));
	}
	
	private Request buildPostRequest(Object json, String apiUrl) throws JsonProcessingException {
		String host = env.getProperty(API_URL);
		String user = env.getProperty(API_USER);
		String key = env.getProperty(API_KEY);
		String endpoint = host + apiUrl;
		ObjectMapper mapper = new ObjectMapper();
		HttpEntity body = new StringEntity(mapper.writeValueAsString(json), ContentType.APPLICATION_JSON);
		return Request.Post(endpoint).addHeader(HEADER_CONTENT_TYPE_JSON).addHeader(new BasicHeader("USER", user))
				.addHeader(new BasicHeader("APIKEY", key)).body(body);
	}

	private void fillBox(BoxListObject box, List<ProductDimensionsObject> productObjectsList) {
		// Test if products fit the box one by one
		Iterator<ProductDimensionsObject> productObjectIterator = productObjectsList.iterator();
		ProductDimensionsObject currentProduct = null;
		boolean canFit = false;
		boolean samePosition = false;
		while (!canFit && productObjectIterator.hasNext()) {
			currentProduct = productObjectIterator.next();
			// Test if the box can fit this product
			if (box.canFitProduct(currentProduct).getCanFit())
				canFit = true;
			samePosition = box.canFitProduct(currentProduct).getNormalPositon();
			if (!canFit) {
				return;
			}
		}
		if (canFit) {
			productObjectsList.remove(currentProduct);
			// split the rest volume to 3 small boxes and recursive call
			List<BoxListObject> smallBoxes = currentProduct.splitBox(box, samePosition);
			if (!smallBoxes.isEmpty()) {
				this.boxFitsProducts(smallBoxes, productObjectsList);
			}
		}
	}

	private final int getSumVolume(List<ProductDimensionsObject> productObjectsList) {
		int sumVolume = 0;
		for (ProductDimensionsObject productObject : productObjectsList) {
			sumVolume += productObject.getVolume();
		}
		return sumVolume;
	}

	private CheckoutSummaryObject checkObject(final Request request, final String url, final CheckoutObject checkoutObject)
			throws Exception {
		return Executor.newInstance().execute(request).handleResponse(r -> {
			int statusCode = r.getStatusLine().getStatusCode();
			if (statusCode == 200 || statusCode == 500 ) {
				ObjectMapper objectMapper = new ObjectMapper();
				return objectMapper.readValue(r.getEntity().getContent(), CheckoutSummaryObject.class);
			}
			LOGGER.error("API " + url + " returned StatusCode[" + statusCode + "] on: " + checkoutObject.toString());
			throw new HttpClientErrorException(HttpStatus.valueOf(statusCode));
		});
	}
}
