package com.deverything.candidate.productstore;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.deverything.candidate.productstore.model.BoxListObject;
import com.deverything.candidate.productstore.model.CheckoutObject;
import com.deverything.candidate.productstore.model.CheckoutSummaryObject;
import com.deverything.candidate.productstore.model.ProductDimensionsObject;
import com.deverything.candidate.productstore.model.ProductObject;

@SpringBootTest
public class ApiServiceTest {

    @Autowired
    ApiService api;
    
    @Test
    public void testAllTheThings() throws Exception{
        System.out.println("Let's get all products from the API:");
        List<ProductObject> products= api.getProducts();
        System.out.println("YOUR-RESULT-HERE \n" + products);
        
        System.out.println("\nLet's list all products with a price higher then 300");
        List<ProductObject> productsPriceHigherThan300 =api.getProductsPriceHigherX(products, 300);
        System.out.println("YOUR-RESULT-HERE \n" + productsPriceHigherThan300);

        System.out.println("\nLet's get product dimensions for products with id 3 and 7 ");
        ProductDimensionsObject productDimensionsObject3=api.getProductDimensions(3);
        ProductDimensionsObject productDimensionsObject7=api.getProductDimensions(7);
        System.out.println("YOUR-RESULT-HERE \n"+"Product3 " + productDimensionsObject3 + " \nProduct7 "+ productDimensionsObject7);
        
        System.out.println("\nGet all boxes and choose the best one that fits both the products 3 and 7 in a single box");
        List<BoxListObject> boxes = api.getBoxes();
        List<ProductDimensionsObject> productObjectsList = new ArrayList<ProductDimensionsObject>();
        productObjectsList.add(productDimensionsObject3);
        productObjectsList.add(productDimensionsObject7);
        BoxListObject boxFitProducts =api.boxFitsProducts(boxes, productObjectsList);
        System.out.println("YOUR-RESULT-HERE ALL Boxes \n"+ boxes);
        System.out.println("\nYOUR-RESULT-HERE Boxes that Fits products \n"+ boxFitProducts);
        
        System.out.println("\nNow we place the order using the checkout in the API");
        List<Integer> productIds = new ArrayList<Integer>();
        productIds.add(3);
        productIds.add(7);
        CheckoutObject.CheckoutObjectBuilder checkoutObject = CheckoutObject.builder()
                .boxId(boxFitProducts.getId())
                .productsIds(productIds);
        CheckoutSummaryObject checkoutSummaryObject = api.checkout(checkoutObject.build());
        System.out.println("YOUR-RESULT-HERE \n" + checkoutSummaryObject);    
    }
   
}